from django.db import models

class Propietario(models.Model):
    rut = models.CharField(max_length = 20)
    nombre = models.CharField(max_length = 20)
    apellido = models.CharField(max_length = 20)
    is_n_ib = models.BooleanField()
    direccion = models.CharField(max_length = 200)
